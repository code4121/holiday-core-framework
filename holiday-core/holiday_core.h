//
//  holiday_core.h
//  holiday-core
//
//  Created by Cody Nelson on 11/13/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for holiday_core.
FOUNDATION_EXPORT double holiday_coreVersionNumber;

//! Project version string for holiday_core.
FOUNDATION_EXPORT const unsigned char holiday_coreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <holiday_core/PublicHeader.h>


