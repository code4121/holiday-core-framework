//
//  UC_PointOfInterest.swift
//  holiday-core
//
//  Created by Cody Nelson on 2/14/18.
//  Copyright © 2018 Bonneville. All rights reserved.
//
import Foundation
import MapKit

public class PointOfInterestEntity : NSObject, MKAnnotation{
    public init(title: String,
         subtitle: String,
         coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
        
    }
    public var title : String?
    public var subtitle : String?
    public var coordinate: CLLocationCoordinate2D
}
