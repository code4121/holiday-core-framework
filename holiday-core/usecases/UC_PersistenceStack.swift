//
//  UC_PersistenceStack.swift
//  holiday-ui
//
//  Created by Cody Nelson on 2/13/18.
//  Copyright © 2018 Cody Nelson. All rights reserved.
//

import Foundation
import MapKit

public protocol UC_PersistenceStack{
    var mapViewControllerDataSource : [PointOfInterestEntity] {get set}
}

