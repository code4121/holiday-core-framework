//
//  PersistenceStackClient.swift
//  holiday-core
//
//  Created by Cody Nelson on 2/14/18.
//  Copyright © 2018 Bonneville. All rights reserved.
//

import Foundation
//
//  PersistenceStackClient.swift
//  holiday-ui
//
//  Created by Cody Nelson on 11/5/17.
//  Copyright © 2017 Cody Nelson. All rights reserved.
//

import Foundation

/// PersistenceStackClient
///
/// Discuss:
/// This object is used to pass state between view controllers in the UI.  When the app is loaded in the main project,
/// it will invesitage all the available view controllers in the app and pass them each the initial state or Persistence
/// Stack object.  For the rest of the application's life, when the view controller segues to another view controller, it
/// must pass the new state to that view controller and set the stack.  If not , the next view controller will not contain
/// updated state.  This process is used, instead of a common singleton, to provide better testing and simple organization.


public protocol PersistenceStackClient {
    
    /// setStack : _
    ///
    /// Discuss:
    /// This method is used to set the persistence stack (or state) at initial loading in the App Delegate, OR it is also used
    /// in the prepareForSegue:_ methods, in each view controller, when the user moves between view controllers.
    
    func setStack(stack: UC_PersistenceStack)
    
    //func setStack(stack: PersistenceStack, didSetStack: (() -> ()))
    
    
    //var didSetStack: (() -> ())? {get set}
}
