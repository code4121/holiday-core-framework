//
//  PersistenceStack.swift
//  SeasonalApp
//
//  Created by Cody Nelson on 11/5/17.
//  Copyright © 2017 Cody Nelson. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import MapKit

/// PersistanceStack
///
/// Discuss:
/// This object contains the state of the app.  Anything that requires state should be stored within this structure.
/// Remember to set the variables to "public" so that they can be seen by the main project.
/// For more information, see : PersistenceStackClient
public class PersistenceStack : UC_PersistenceStack{
    
    public init( mapDataSource : [PointOfInterestEntity] ) {
        self.mapViewControllerDataSource = mapDataSource
    }
    
    /// Used as the data source for map points in the MapTab's Map View Controller.
    public var mapViewControllerDataSource : [PointOfInterestEntity]
    
}



